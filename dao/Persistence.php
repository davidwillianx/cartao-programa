<?php

require dirname(__DIR__).'/vendor/autoload.php';

class Persistence
{
	/**
	 * [$pdoConnection variavel que armazena a instancia de PDO - not sigleton]
	 * @var [PDO]
	 */
	private $pdoConnection;

	/**
	 * [ Resultado da pré-compilar do query retorna a instancia de STM ]
	 * @var [PreparedSteatment]
	 */
	private $stmt;
	/**
	 * [$sqlQuery description]
	 * @var [String]
	 */
	private $sqlQuery;
	/**
	 * [$indexBind description]
	 * @var [type]
	 */
	private $indexBind;
	

	/**
	 * [Persistence requisita instancia de PDO e armazena pdoConnection
	 */
	public function Persistence()
	{
		try{
			$this->pdoConnection = new PDO(
			 Configuration::getDnsConnection()
			,Configuration::USER
			,Configuration::PASSWD
			);
		}catch(PDOException $error)
		{
			echo 'Connection Failure';	
		}
		
	}

	/**
	 * [selectPolyline responsável por selecionar as polilinhas]
	 * @param  int $cardId
	 * @return array polylines
	*/
	public function selectPolyline($cardId)
	{
		$this->sqlQuery = "SELECT l.* FROM
							 card c JOIN map m ON (c.idMap = m.id) 
							 	JOIN polyline p ON (p.idMap = m.id) 
							 	JOIN polylineLatLng pll ON (pll.idPolyline = p.id)
							 	JOIN latLng l ON (l.id = pll.idLatLng) WHERE c.id = ?";

		return $this->selectDataFromQueryById($cardId);
	}

	/**
	 * [selectPolygon responsável por selecionar as poligonos]
	 * @param  int $cardId
	 * @return array polygons
	 */
	public function selectPolygon($cardId)
	{
		$this->sqlQuery = "SELECT l.* FROM
							 card c JOIN map m ON (c.idMap = m.id) 
							 	JOIN polygon p ON (p.idMap = m.id) 
							 	JOIN polygonLatLng pll ON (pll.idPolygon = p.id)
							 	JOIN latLng l ON (l.id = pll.idLatLng) WHERE c.id = ?";
		return $this->selectDataFromQueryById($cardId);			

	}

	/**
	 * [selectMarkers responsável por selecionar os marcadores]
	 * @param  int $cardId
	 * @return array markers
	 */
	public function selectMarkers($cardId)
	{
		$this->sqlQuery = "SELECT m.arrival, m.description, m.leave, lt.lat, lt.lng 
		       FROM card c JOIN map ON (map.id = c.idMap) JOIN marker m 
		       ON (map.id = m.idMap) JOIN latLng lt ON (m.idLatLng = lt.id) 
		       WHERE c.id = ?";
		return $this->selectDataFromQueryById($cardId);       
	}

	/**
	 * [responsável por selecionar os marcadores globais]
	 * @return array globalMarkers
	 * 
	 */
	public function selectGlobalMarkers()
	{
		$this->sqlQuery = "SELECT g.description, g.lat, g.lng FROM markerGlobal g";		       
		$this->prepare($this->sqlQuery);
		
		$this->stmt->execute();

		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	*@method delete  responsável por remover os marcadores globais
	*@param MarkerGlobal $markerGlobal
	*/

	/**
	 * [responsável por remover os marcadores globais]
	 * @param  MarkerGlobal $markerGlobal
	 * @return Bool result
	 */
	public function delete(MarkerGlobal $markerGlobal)
	{
		$this->sqlQuery = 'DELETE FROM markerGlobal WHERE lat=? AND lng=?';

		$this->prepare($this->sqlQuery);

		$this->setParam($markerGlobal->getLat());
		$this->setParam($markerGlobal->getLng());
		
		return $this->stmt->execute();

	}

	/**
	 * [responsável por selecionar os cartões programa]
	 * @param  [type] $cardId
	 * @return [type]
	 */
	public function selectCard($cardId)
	{
		$this->sqlQuery = 'SELECT cardDescription , roteDescription,
		                     areaDescription  FROM card  WHERE card.id = ?';
             
		return $this->selectDataFromQueryById($cardId);                      
	}

	/**
	 * [Responsável por selecionar o mapa]
	 * @param  int $cardId
	 * @return array map
	 */
	public function selectMap($cardId)
	{
		$this->sqlQuery = 'SELECT m.*, l.lat,l.lng FROM card c 
							JOIN map m ON (c.idMap = m.id) 
							JOIN latLng l ON (m.idLatlng = l.id) 
								 WHERE c.id = ?';

		return $this->selectDataFromQueryById($cardId);
	}

	/**@TODO verificar a dependencia desse método e remove-lo*/
	/**
	 * [executa operação ]
	 * @param  int $cardId
	 * @return [type]
	 */
	private function selectDataFromQueryById($cardId)
	{
		try{	
			$this->prepare($this->sqlQuery);
			$this->setParam($cardId);

			$this->stmt->execute();

			$search = $this->stmt->fetchAll(PDO::FETCH_ASSOC);

			$this->stmt->closeCursor();
			
			return  $search;

		}
		catch(Exception $e)
		{
			echo 'Erro na consulta'.$e->getMessage.'';
		}
	}

	/**
	*@method persistMap  realiza a persistência do mapa
	*@param Map $map
	*
	*/
	public function persistMap(Map $map)
	{
		try {
			
			$latLng = new LatLng();
			
			$latLng->setLat($map->getLat());
			$latLng->setLng($map->getLng());
			
			$idLatLng = $this->persistLastId($latLng);
			$map->setIdLatLng($idLatLng);

			return $this->persistLastId($map);

		} catch (Exception $error) {
			
		}
	}

	/**
	*@method persistMarkers  realiza a persistência dos marcadores
	*@param Markers $marker
	*
	*/
	public function persistMarkers($markers,$idMap)
	{
		try {
		
			foreach ($markers as $marker) {
			$latLng = new LatLng();
			$latLng->setLat($marker->getLat());
			$latLng->setLng($marker->getLng());

			$idLatLng = $this->persistLastId($latLng);
			$marker->setIdLatLng($idLatLng);
			$marker->setIdMap($idMap);

			$result  = $this->persist($marker);	
		}

		} catch (Exception $e) {
			echo 'Problema  em salvar os markers';
		}	
	}

	/**
	*@method persistPolygonListLatLng realiza a persistencia na tabela de realacionamento entre latitude e poligono
	*@param $listLatLng $idPolygon
	*@REFACTORY realizar a contagem de latLngTotal e persistidas para avaliar se todas as informações foram salvas
	*/
	public function persistPolygonListLatLng($listLatLng,$idPolygon)
	{
		foreach ($listLatLng as $latLng) {
			$idLatLng = $this->persistLastId($latLng);
			$polygonLatLng = new PolygonLatLng();
			$polygonLatLng->setIdPolygon($idPolygon);
			$polygonLatLng->setIdLatLng($idLatLng);
			$this->persist($polygonLatLng);
		}
	}

	/**
	*@method persistPolylineListLatLng  realiza a persistencia da tabela de realacionamento entre polilinhas e latitude 
	*@param $listLatLng $idPolyline 
	*@REFACTORY realizar a contagem de latLngTotal e persistidas para avaliar se todas as informações foram salvas
	*/
	public function persistPolylineListLatLng($listLatLng,$idPolyline)
	{
		foreach ($listLatLng as $latLng) {
			$idLatLng = $this->persistLastId($latLng);
			$polylineLatLng = new PolylineLatLng();
			$polylineLatLng->setIdPolyline($idPolyline);
			$polylineLatLng->setIdLatLng($idLatLng);
			$this->persist($polylineLatLng);
		}
	}

		/*REFACTORY realizar a contagem de latLngTotal e persistidas 
		para avaliar se todas as informações foram salvas
	*/

	/**
	*@mehod prepare realiza  leitura da query para executar a transação
	*@param $sqlQuery 
	*@see http://php.net/manual/pt_BR/pdo.prepare.php
	*@return null
	*/
	public function prepare($sqlQuery)
	{
		$this->stmt = $this->pdoConnection->prepare($sqlQuery);
		$this->indexBind = 0;
	}

	/**
	*@mehod setParam realiza  a união entre os atributos dinâmicos da query e seus respectivos valores
	*@param $value
	*@see http://php.net/manual/pt_BR/pdostatement.bindparam.php
	*@return null
	*/
	public function setParam($value)
	{
		$this->stmt->bindParam(++$this->indexBind,$value);
	}

	/**
	*@method persist  metodo responsavel pela  persistencia em banco de dados
	*@param $objectInstance
	*@TODO Throw exceptions Reflect (Class/Method)
	*@TODO Remove Id attribute into classReflected
	*@throws Exception
	*/
	public function persist($objectInstance)
	{
		try{

			$buildSqlQuery = new BuildSqlQuery($objectInstance);
			$sqlInsert = $buildSqlQuery->insertQuery();
	 		$this->prepare($sqlInsert);
	 		
			$reflectionClass =  new ReflectionClass($objectInstance);
			$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);
			

			foreach ($properties as $property) {
				if($property->getName() != 'id')
				{
					$reflectionMethod =  new ReflectionMethod($objectInstance,'get'.ucwords($property->getName()));
					$this->setParam($reflectionMethod->invoke($objectInstance));
				}
			}

			return $this->stmt->execute();

		}catch(Exception $error){
			echo 'Algum error ocorreu depois vamos melhorar o tratamento'.$error->getMessage();
		}
	}

	/**
	*@method persistLastId realiza a operação no banco de dados e retorna a ultima chave inserida
	*@param $objectInstance
	*@return lastIdInserted
	*/
	public function persistLastId($objectInstance)
	{
		$this->persist($objectInstance);
		return $this->pdoConnection->lastInsertId();
	}

	/**
	*@method select
	*@param $objectInstance
	*@return array result
	*/
	public function select($objectInstance)
	{
		$buildSqlQuery = new BuildSqlQuery($objectInstance);
		$sqlSelect = $buildSqlQuery->selectQuery();

		$this->prepare($sqlSelect);
		$this->stmt->execute();
		return $this->stmt->fetchAll(PDO::FETCH_CLASS,strtolower(get_class($objectInstance)));

	}


	/**
	*@method selectAllCardWithDescription
	*@param $card,$where
	*@return array result
	*@throws Exception
	*/
	public function  selectAllCardWithDescription($card,$where)
	{
		try {
				
			$this->sqlQuery = 'SELECT id, areaDescription, roteDescription, cardDescription, created, modified, idMap FROM card WHERE cardDescription LIKE ? ';
			$this->prepare($this->sqlQuery);

			$this->setParam("%".$card->getCardDescription()."%");
			$this->stmt->execute();

			return $this->stmt->fetchAll(PDO::FETCH_CLASS,strtolower(get_class($card)));

		} catch (Exception $e) {
			echo 'problema ao buscar os valores do banco de dados';
		}
	}

	/**
	*@method selectAllWithWhere
	*@param $objectInstance, $where
	*@return array result 
	*/
	public function selectAllWithWhere($objectInstance,$where){
		$this->selectWhere($objectInstance,$where);
		return $this->stmt->fetchAll(PDO::FETCH_CLASS,strtolower(get_class($objectInstance)));
	}

	/**
	*@method selectAllWithWhere
	*@param $objectInstance, $where
	*@return Object result 
	*/
	public function selectWithWhere($objectInstance,$where)
	{
		$this->selectWhere($objectInstance,$where);
		return $this->stmt->fetchObject(strtolower(get_class($objectInstance)));
	}

	/**
	 * [selectWhere description]
	 * @param  Object $objectInstance
	 * @param  String $where
	 * @return Bool executed
	 */
	private function selectWhere($objectInstance,$where)
	{
		try
		{
			$buildSqlQuery = new BuildSqlQuery($objectInstance);
			$sqlSelect = $buildSqlQuery->selectQueryWithWhere($where);
				
				$this->prepare($sqlSelect);

			if($where)
			{
				$where = explode(' and ',$where);
				$where = implode($where);
				$properties = explode('= ?',$where);	
				array_pop($properties);

			
				foreach ($properties as $property)
				{	
					$reflectionMethod =  new ReflectionMethod($objectInstance,'get'.ucwords($property));
					$this->setParam($reflectionMethod->invoke($objectInstance));
				}
			}

			$this->stmt->execute();
			

		}catch(Exception $error)
		{
			echo 'Algum error ocorreu depois vamos melhorar o tratamento';
		}
		
	}

	/**
	 * [updateMarkerGlobal description]
	 * @param  array $markerGlobal
	 * @param  array $oldLat
	 * @param  array $oldLng
	 * @return Bool result
	 */
	public function updateMarkerGlobal($markerGlobal,$oldLat,$oldLng)
	{
		$this->sqlQuery = 'UPDATE markerGlobal set description = ? , lat = ? , lng = ?
									WHERE lat = ? AND lng = ?';
		$this->prepare($this->sqlQuery);

		$this->setParam($markerGlobal->getDescription());
		$this->setParam($markerGlobal->getLat());
		$this->setParam($markerGlobal->getLng());

		$this->setParam($oldLat);
		$this->setParam($oldLng);

		return $this->stmt->execute();
	}
}