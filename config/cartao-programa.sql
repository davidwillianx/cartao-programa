SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `cartaoprograma` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `cartaoprograma` ;

-- -----------------------------------------------------
-- Table `cartaoprograma`.`latLng`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`latLng` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `lat` FLOAT NOT NULL ,
  `lng` FLOAT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`map`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`map` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `zoom` INT(55) NOT NULL ,
  `idLatlng` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_map_latlng1` (`idLatlng` ASC) ,
  CONSTRAINT `fk_map_latlng1`
    FOREIGN KEY (`idLatlng` )
    REFERENCES `cartaoprograma`.`latLng` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`marker`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`marker` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(45) NULL ,
  `arrival` VARCHAR(45) NULL ,
  `leave` VARCHAR(45) NULL ,
  `idLatLng` INT NOT NULL ,
  `idMap` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_marker_latlng1` (`idLatLng` ASC) ,
  INDEX `fk_marker_map1` (`idMap` ASC) ,
  CONSTRAINT `fk_marker_latlng1`
    FOREIGN KEY (`idLatLng` )
    REFERENCES `cartaoprograma`.`latLng` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_marker_map1`
    FOREIGN KEY (`idMap` )
    REFERENCES `cartaoprograma`.`map` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`polyline`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`polyline` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `idMap` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_polyline_map1` (`idMap` ASC) ,
  CONSTRAINT `fk_polyline_map1`
    FOREIGN KEY (`idMap` )
    REFERENCES `cartaoprograma`.`map` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`polylinePath`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`polylinePath` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `idPolyline` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_polylinePath_polyline` (`idPolyline` ASC) ,
  CONSTRAINT `fk_polylinePath_polyline`
    FOREIGN KEY (`idPolyline` )
    REFERENCES `cartaoprograma`.`polyline` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`polygon`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`polygon` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `idMap` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_polygon_map1` (`idMap` ASC) ,
  CONSTRAINT `fk_polygon_map1`
    FOREIGN KEY (`idMap` )
    REFERENCES `cartaoprograma`.`map` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`polygonPath`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`polygonPath` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `idPolygon` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_polygonPath_polygon1` (`idPolygon` ASC) ,
  CONSTRAINT `fk_polygonPath_polygon1`
    FOREIGN KEY (`idPolygon` )
    REFERENCES `cartaoprograma`.`polygon` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`card`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`card` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `areaDescription` VARCHAR(45) NOT NULL ,
  `roteDescription` VARCHAR(45) NULL ,
  `cardDescription` VARCHAR(45) NULL ,
  `created` DATETIME NULL ,
  `modified` DATETIME NULL ,
  `idMap` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_card_map1` (`idMap` ASC) ,
  CONSTRAINT `fk_card_map1`
    FOREIGN KEY (`idMap` )
    REFERENCES `cartaoprograma`.`map` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`latLngPolygonPath`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`latLngPolygonPath` (
  `idLatlng` INT NOT NULL ,
  `idPolygonPath` INT NOT NULL ,
  PRIMARY KEY (`idLatlng`, `idPolygonPath`) ,
  INDEX `fk_latlng_has_polygonPath_polygonPath1` (`idPolygonPath` ASC) ,
  INDEX `fk_latlng_has_polygonPath_latlng1` (`idLatlng` ASC) ,
  CONSTRAINT `fk_latlng_has_polygonPath_latlng1`
    FOREIGN KEY (`idLatlng` )
    REFERENCES `cartaoprograma`.`latLng` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_latlng_has_polygonPath_polygonPath1`
    FOREIGN KEY (`idPolygonPath` )
    REFERENCES `cartaoprograma`.`polygonPath` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`latLngPolylinePath`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`latLngPolylinePath` (
  `idPolylinePath` INT NOT NULL ,
  `idLatlng` INT NOT NULL ,
  PRIMARY KEY (`idPolylinePath`, `idLatlng`) ,
  INDEX `fk_polylinePath_has_latlng_latlng1` (`idLatlng` ASC) ,
  INDEX `fk_polylinePath_has_latlng_polylinePath1` (`idPolylinePath` ASC) ,
  CONSTRAINT `fk_polylinePath_has_latlng_polylinePath1`
    FOREIGN KEY (`idPolylinePath` )
    REFERENCES `cartaoprograma`.`polylinePath` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_polylinePath_has_latlng_latlng1`
    FOREIGN KEY (`idLatlng` )
    REFERENCES `cartaoprograma`.`latLng` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cartaoprograma`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cartaoprograma`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
