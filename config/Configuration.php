<?php

	/**
	* @author #PirateTeam
	* class responsável por fornecer os dados da conexão com 
	* a base de dados MYSQL
	*/
	class Configuration
	{
	    /*
	    @Develop evironment*/
		const HOSTCONNECT = 'localhost';
		const TYPEDB = 'mysql';
		const DBNAME = 'cartaoprograma';
		const USER = 'root';
		const PASSWD = '';

		/*
		@Deploy environment 
			#put here 
		*/
 
		/** 
		 *getDnsConnection()
		 *@return Path Connection
		*/
		public static function getDnsConnection()
		{
			return self::TYPEDB.':dbname='.self::DBNAME.';host='.self::HOSTCONNECT;
		}
	}
 ?>