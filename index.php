 <?php
	
	include_once 'view/template/head.php';	
    
    include_once 'controllers/ControllerPessoa.php';
    $controllerPessoa  = new ControllerPessoa();
    $controllerPessoa->checkAuthenticate(new Request());

 ?>

 <div class="container well">
      <form action="" method="post">    
      
        <div class="control-group">
          <label for="id_username">Usuário</label>
          <div class="controls">
            <input id="id_username" maxlength="254" name="username" type="text">
            <span class="help-inline"></span>
          </div>
        </div>
      
        <div class="control-group">
          <label for="id_password">Senha</label>
          <div class="controls">
            <input id="id_password" name="password" type="password">
            <span class="help-inline"></span>
          </div>
        </div>
      
      <input type="hidden" name="next" value="/home/">
      <input type="submit" class="btn btn-primary" name="userRequestAccess" value="Login">
    </form> 
  </div>