$ ->
  divMap = document.getElementById("map")
  drawingManager = new google.maps.drawing.DrawingManager
    drawingMode: null
    drawingControl: true
    drawingControlOptions:
      position: google.maps.ControlPosition.BOTTOM_LEFT,
      drawingModes: [
        google.maps.drawing.OverlayType.MARKER,
        google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.POLYGON
      ]
    markerOptions:
      draggable: true
    polylineOptions:
      editable: true
      strokeColor: 'green'
      icons: [{
        icon: {
          path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
        },
        offset: '10%'
        repeat: '20%'
      }]
    polygonOptions:
      draggable: true
      editable: true
      fillColor: 'red'
      strokeColor: 'red'

  # Esse teste é necessário para executar esse script normalmente em outras páginas
  # que não contém a div referida.
  if divMap
    google.maps.visualRefresh = true

    mapOptions =
      center: new google.maps.LatLng(-1.4558333333, -48.5027777778)
      zoom: 15
      mapTypeId: google.maps.MapTypeId.ROADMAP

    configure = (options) ->
      map = new google.maps.Map(divMap, mapOptions)
      drawingManager.setMap(map)
      map

    map = configure(mapOptions)

    # Repositório dos objetos criados pelo drawningManager. Com ele construo os dados para
    # enviar ao servidor.
    register =
      map: map
      markers: []
      polylines: []
      polygons: []

    # Cada marcador possui metadata associada mas tudo é referenciado usando a posição do marcador na lista
    # markers (que é única).
    markerAdded = (marker) ->
      infoWindow = new google.maps.InfoWindow
        zIndex: 5
      register.markers.push marker
      position = register.markers.indexOf(marker)

      localStorage["description#{position}"] = ""
      localStorage["arrival#{position}"] = ""
      localStorage["leave#{position}"] = ""

      $("div#map").on "click.event#{position}", "button#save#{position}", ->
        localStorage["description#{position}"] = $("#description#{position}").val()
        localStorage["arrival#{position}"] = $("#arrival#{position}").val()
        localStorage["leave#{position}"] = $("#leave#{position}").val()
        alert 'Dados do ponto salvos armazenados com sucesso.'
        console.log localStorage

      $("div#map").on "click.event#{position}", "button#delete#{position}", ->
        if confirm('Deletar esse ponto?')
          register.markers[position].setMap null
          register.markers[position] = null
          localStorage.removeItem("description#{position}")
          localStorage.removeItem("arrival#{position}")
          localStorage.removeItem("leave#{position}")
          $('div#map').off('.event#{position}')

      $("#description#{position}").livequery ->
        $(@).focus()

      open = false
      handlerClick = ->
        if not open
          infoWindow.setContent """
            <div id="infoWindow#{position}">
              <p>Descrição</p>
              <p><textarea id="description#{position}">#{localStorage["description#{position}"]}</textarea></p>
              <p>Chegada</p>
              <p><input id="arrival#{position}" type="text" value='#{localStorage["arrival#{position}"]}' class="datetimepicker"/></p>
              <p>Saída</p>
              <p><input id="leave#{position}" type="text" value='#{localStorage["leave#{position}"]}' class="datetimepicker"/></p>
              <p>
                <button id="save#{position}" class="btn">Salvar</button>
                <button id="delete#{position}" class="btn">Deletar</button>
              </p>
            </div>
          """
          infoWindow.open(map, marker)
          $.datepicker.setDefaults
            dateFormat: 'dd/mm/yy'
          $('.datetimepicker').datetimepicker
            timeFormat: 'HH:mm:ss'
          open = true
          closeClick = ->
            open = false
          google.maps.event.addListener(infoWindow, 'closeclick', closeClick)
      google.maps.event.addListener(marker, 'click', handlerClick)

    polylineAdded = (polyline) ->
      register.polylines.push polyline
    polygonAdded = (polygon) ->
      register.polygons.push polygon

    google.maps.event.addListener(drawingManager, 'markercomplete', markerAdded)
    google.maps.event.addListener(drawingManager, 'polylinecomplete', polylineAdded)
    google.maps.event.addListener(drawingManager, 'polygoncomplete', polygonAdded)

    clearMarkers = ->
      register.markers.forEach (marker) ->
        if marker
          position = register.markers.indexOf(marker)
          marker.setMap(null)
          register.markers[position] = null
      localStorage.clear()
    clearMarkers()

    $('#clear-markers').click ->
      if confirm('Remover marcadores?')
        clearMarkers()

    clearPolylines = ->
      register.polylines.forEach (polyline) ->
        polyline.setMap(null)
      register.polylines.length = 0
    clearPolylines()

    $('#clear-polyline').click ->
      if confirm('Remover rota?')
        clearPolylines()

    clearPolygons = ->
      register.polygons.forEach (polygon) ->
        polygon.setMap(null)
      register.polygons.length = 0
    clearPolygons()

    $('#clear-polygon').click ->
      if confirm('Remover área?')
        clearPolygons()

    show = false
    heatmap = new google.maps.visualization.HeatmapLayer
    $('#heatmap').click ->
      t = $(@)
      $.getJSON '/heatmap/', (data) ->
        if not show
          data = for d in data
            new google.maps.LatLng(d.lat, d.lng)
          heatmap.setMap map
          heatmap.setData data
          show = true
          t.html "Ocultar mancha"
        else
          heatmap.setMap null
          heatmap.setData
          show = false
          t.html "Mancha"
    $('#save-map').click ->
      if not $('#card-description').val()
        alert 'Preencha a descrição do cartão'
        return false

      if not $('#area-description').val()
        alert 'Preencha a descrição da área'
        return false

      if not $('#rote-description').val()
        alert 'Preencha a descrição da rota'
        return false

      if confirm('Salvar cartão?')
        data =
          map:
            center:
              lat: register.map.getCenter().lat()
              lng: register.map.getCenter().lng()
            zoom: register.map.getZoom()
          cardDescription: $('#card-description').val()
          areaDescription: $('#area-description').val()
          roteDescription: $('#rote-description').val()
          markers: for marker in register.markers when marker isnt null
              {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng(),
                arrival: localStorage["arrival#{register.markers.indexOf(marker)}"],
                leave: localStorage["leave#{register.markers.indexOf(marker)}"],
                description: localStorage["description#{register.markers.indexOf(marker)}"]
              }
          polylines: for polyline in register.polylines
            {
              path: for position in polyline.getPath().getArray()
                {lat: position.lat(), lng: position.lng()}
            }
          polygons: for polygon in register.polygons
            {
              path: for position in polygon.getPath().getArray()
                {lat: position.lat(), lng: position.lng()}
            }
          sigilo: $('#sigilo').val()

        console.log data

        $.ajax
          url: '/salvar/cartão/'
          cache: false
          type: 'post'
          dataType: 'json'
          data: JSON.stringify(data)

          beforeSend: (xhr, settings) ->
              if !csrfSafeMethod(settings.type)
                  xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))

              $.blockUI
                message: 'Salvando cartão, aguarde...'
                css:
                  border: 'none'
                  padding: '15px'
                  backgroundColor: '#000'
                  '-webkit-border-radius': '10px' 
                  '-moz-border-radius': '10px'
                  opacity: .5
                  color: '#fff'
                  
          success: (data) ->
            $.unblockUI()
            alert "Cartão salvo com sucesso."
            window.location = '/novo/cartão/'

  if $('h3#card').length
    $.getJSON "/carregar/cartão/#{$('h3#card').data('pk')}", (data) ->
      console.log data
      google.maps.visualRefresh = true
      mapOptions =
        center: new google.maps.LatLng(data.map.center.lat, data.map.center.lng)
        zoom: data.map.zoom
        mapTypeId: google.maps.MapTypeId.ROADMAP

      map = configure(mapOptions)

      register.map = map
      for marker in data.markers
        m = new google.maps.Marker
          position: new google.maps.LatLng(marker.lat, marker.lng)
          map: map
          draggable: true
        markerAdded(m)

        position = register.markers.indexOf(m)
        localStorage["description#{position}"] = marker.description
        localStorage["arrival#{position}"] = marker.arrival
        localStorage["leave#{position}"] = marker.leave
        
      data.polylines.forEach (polyline) ->
        path = for l in polyline.path
          new google.maps.LatLng(l.lat, l.lng)

        addRow = (ll, i) ->
          $.getJSON "http://maps.googleapis.com/maps/api/geocode/json?latlng=#{ll.lat()},#{ll.lng()}&sensor=false", (data) ->
            $("<tr><td>#{i+1} #{ll}</td><td>#{data.results[0].formatted_address}</td></th>").appendTo $('#geo tbody')
                
        for ll, i in path
          addRow ll, i
        p = new google.maps.Polyline
          path: path
          map: map
          strokeColor: 'green'
          editable: true
          icons: [{
            icon: {
              path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
            },
            offset: '10%'
            repeat: '20%'
          }]
        polylineAdded(p)

      data.polygons.forEach (polygon) ->
        path = for l in polygon.path
          new google.maps.LatLng(l.lat, l.lng)
        po = new google.maps.Polygon
          path: path
          map: map
          draggable: true
          editable: true
          fillColor: 'red'
          strokeColor: 'red'
        polygonAdded(po)


  getCookie = (name) ->
    cookieValue = null
    if document.cookie && document.cookie != ''
      cookies = document.cookie.split(';')
      for c in cookies
        cookie = $.trim(c)
        if cookie.substring(0, name.length + 1) == (name + '=')
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break
    cookieValue

  csrfSafeMethod = (method) ->
    /^(GET|HEAD|OPTIONS|TRACE)$/.test(method)

  $('.span2 a').css
    display: 'block'