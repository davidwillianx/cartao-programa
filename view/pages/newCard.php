<?php 

  include_once dirname(__DIR__).'/template/menu.php';
  include_once '../../controllers/ControllerPessoa.php';
  $controllerPessoa = new ControllerPessoa();
  $controllerPessoa->securityPages();
?>
<div class="container">
    <div class="row">
      <div class="span10">
       
        <label for="card-description">Descrição do cartão</label>
        <textarea id="card-description" style="width: 80%"></textarea>

        <label for="area-description">Descrição da área</label>
        <textarea id="area-description" style="width: 80%"></textarea>

        <label for="rote-description">Descrição da rota</label>
        <textarea id="rote-description" style="width: 80%"></textarea>

        <div class="btn-group" data-toggle="buttons-radio" style="margin-bottom: 15px">
          <button type="button" class="btn drawing-control" data-type="">Mãozinha</button>
          <button type="button" class="btn drawing-control" data-type="m">Marcador</button>
          <button type="button" class="btn drawing-control" data-type="g">Global</button>
          <button type="button" class="btn drawing-control" data-type="r">Rota</button>
          <button type="button" class="btn drawing-control" data-type="a">Área</button>
        </div>
        <div>
            <div class="btn-group">
              <button id="heatmap" class="btn headmap" data-url="/heatmap/" data-name="Todas">Mancha (Todas)</button>
              <button class="btn dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="/heatmap/" class="heatmap" data-name="Todas">Todas</a></li>
                <li><a href="/heatmap/" class="heatmap" data-name="Todas">sequesto</a></li>
                <li><a href="/heatmap/" class="heatmap" data-name="Todas">furto</a></li>
                <li><a href="/heatmap/" class="heatmap" data-name="Todas">assalto</a></li>
                <li><a href="/heatmap/" class="heatmap" data-name="Todas">tráfico</a></li>
              </ul>
            </div>
            <button id="globals" class="btn" data-hide="y">Toggle globals</button>
        </div>

        <div id="map" class="img-polaroid"></div>
        <table id="geo" class="table table-bordered table-hovered" style="position: relative; top: 30">
          <thead>
            <tr><td>Ponto</td><td>Ruas</td></tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      <div class="span2">
        
        <a id="clear-markers" class="btn">Limpar marcadores</a>
        <a id="clear-polyline" class="btn">Limpar rota</a>
        <a id="clear-polygon" class="btn">Limpar área</a>
        <a id="heatmap" class="btn">Mancha</a>
        <a id="save-map" class="btn">Salvar</a>
      </div>
    </div>
  </div>
