<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<title>Bem vindo</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="view/styles/bootstrap.min.css">
	<link rel="stylesheet" href="view/styles/js/external/jquery-ui-1.10.3.custom.min.css"/>
	<link rel="stylesheet" href="view/styles/main.css"/>
	<link rel="stylesheet" href="view/styles/jquery.multiselect.css"/>
   	<script src="http://maps.google.com/maps/api/js?v=3&amp;libraries=drawing,visualization&amp;sensor=false&amp;language=pt&amp;region=BR"></script>



	<script src="view/styles/js/external/jquery-2.0.0.min.js"></script>
	<script src="view/styles/js/external/bootstrap.min.js"></script>
	<script src="view/styles/js/external/jquery-ui-1.10.3.custom.min.js"></script>
	<!--<script src="view/styles/js/external/jquery-ui-timepicker-addon.js"></script>-->
	<!--<script src="view/styles/js/external/jquery-ui-sliderAccess.js"></script>-->
	<script src="view/styles/js/external/jquery.livequery.min.js"></script>
	<script src="view/styles/js/external/jquery.blockUI.js"></script>
	<script src="view/styles/js/main.min.js"></script>
	<script src="view/styles/js/register.min.js"></script>
	<script src="view/styles/js/external/jquery.multiselect.min.js"></script>

	
</head>