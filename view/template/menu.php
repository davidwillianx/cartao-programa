<? 
	include_once '../../library/IncludeFiles.php';
  IncludeFiles::pickUpTemplate('head');

	include_once '../../controllers/ControllerPessoa.php';

  $controllerPessoa = new ControllerPessoa();
  $controllerPessoa->securityPages();
	$user = $controllerPessoa->getDataUserAuthenticated(new Request());
?>
   <div class="navbar container navbar-inverse">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="./dashboard">Cartão Programa</a>

          <div class="nav-collapse collapse navbar-responsive-collapse">
            <ul class="nav pull-right">
              <li><a href="dashboard">Home</a></li>
              <li ><a href="./new">Cartão</a></li>
              <li ><a href="./search">Procurar</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php  echo $user->getUsername(); ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="./logout">Logout</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- /.nav-collapse -->
        </div>
      </div>