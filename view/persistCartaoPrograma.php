<?php
 include_once '../library/Request.php';
 include_once '../entity/Map.php';
 include_once '../entity/Marker.php';
 include_once '../controllers/ControllerCartaoPrograma.php';
 
 $controllerCartaoPrograma = new ControllerCartaoPrograma();
 $request = new Request();

 session_start();
 $request->set('idUser',$_SESSION['userId']);
 session_write_close();
 $controllerCartaoPrograma->save($request);

 ?>