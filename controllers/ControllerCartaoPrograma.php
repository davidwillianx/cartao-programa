<?php

	/**
	*@author #PirateTeam
	*Class responsável por realizaçao a comunicação
	* e transação de informações do cartão programa
	*/

	require dirname(__DIR__).'/vendor/autoload.php';

	class  ControllerCartaoPrograma
	{
		/**
		 * [lazzyload para a class]
		 * @var Persistence
		 */
		private $persistenceConnection;

		/**
		*@method getPersistece
		*Tem como propósito retornar a conexão ativa.
		*@return $persistenceConnectio
		*/
	
		/**
		 * [getPersistence description]
		 * @return Persistence 
		 */
		public function getPersistence()
		{	
			if(!$this->persistenceConnection)
				$this->persistenceConnection = new Persistence();
			return $this->persistenceConnection;
		}

		/**
		 * [Obter informações da entidade map pela UX e persistir em banco de dados]
		 * @param  Request $request
		 * @param  array $idLatLng
		 * @return int idMap
		 */
		public function saveMap($request,$idLatLng)
		{
			$map = $request->buildObjectToKey(new Map(),'map');
			$map->setIdLatLng($idLatLng);
			
			return $this->getPersistence()->persistLastId($map);		
		}

		/**
		 * [Obter informações da entidade map pela UX e persistir em banco de dados sua latitude e]
		 * @param  Request $request
		 * @return int idMapLatLng
		 */
		public function saveMapLatLng($request)
		{
			$latLng = $request->listLatLng('map');
			return $this->getPersistence()->persistLastId($latLng[0]);

		}
	
		/**
		 * [Obter informações da entidade card pela UX e persistir em banco de dados]
		 * @param  Request $request
		 * @param   int $idMap
		 * @return Boolean
		 */
		public function saveCard($request,$idMap)
		{
			$card = new Card();
			$card->setCardDescription($request->getKey('cardDescription'));
			$card->setAreaDescription($request->getKey('areaDescription'));
			$card->setRoteDescription($request->getKey('roteDescription'));
			$card->setIdMap($idMap);
			$card->setIdUser($request->getKey('idUser'));

			$this->getPersistence()->persist($card);
		}

		/**
		 * [Obter informações da entidade markers pela UX e persistir em banco de dados]
		 * @param  Request $request
		 * @param  int $idMap
		 * @return Boolean
		 */
		public function saveMarkers($request,$idMap)
		{
			$markers = $request->buildMarkers(new Marker(),'markers');
			$this->getPersistence()->persistMarkers($markers,$idMap);
		}

		/**
		*@method savePolyline
		*Obter informações da entidade map pela UX e persistir em banco de dados
		*@return $idPolylinePersisted
		*@param $idPolyline
		*@TODO addicionar bloco de excessao
		*/
		public function savePolyline($idMap)
		{
			$polyline = new Polyline();
			$polyline->setIdMap($idMap);

			return $this->getPersistence()->persistLastId($polyline);

		}

		/**
		*@method savePolylineLatLng
		*Obter informações da entidade latLng pela UX e persistir em banco de dados
		*@param $request, $idPolyline
		*@TODO addicionar bloco de excessao
		*/
		public function savePolylineLatLng($request,$idPolyline)
		{
			$latLng = $request->listLatLng('polyline');
			$this->getPersistence()->persistPolylineListLatLng($latLng,$idPolyline);
		}

		/**
		*@method savePlygon
		*Obter informações da entidade polygon pela UX e persistir em banco de dados
		*@return $idPolygon
		*@param $idMap
		*@TODO addicionar bloco de excessao
		*/
		public function savePolygon($idMap)
		{
			$polygon =  new Polygon();
			$polygon->setIdMap($idMap);

			return $this->getPersistence()->persistLastId($polygon);
		}

		/**
		*@method saveMap
		*Obter informações da entidade latLng pela UX e persistir em banco de dados
		*@param $request, $idPolygon
		*@TODO addicionar bloco de excessao
		*/
		public function savePolygonLatLng($request,$idPolygon)
		{
			$latLng = $request->listLatLng('polygon');
			$this->getPersistence()->persistPolygonListLatLng($latLng,$idPolygon);
		}

		/**
		*@param $request
		*Obter informações da UX e persistir em banco de dados
		*@return json_encode($transac)
		*@TODO addicionar bloco de excessao
		*/
		public function save(Request $request)
		{
			try {

				$idLatLng = $this->saveMapLatLng($request);
				$idMap = $this->saveMap($request,$idLatLng);
				$this->saveCard($request,$idMap);
				$this->saveMarkers($request,$idMap);
				$idPolyline = $this->savePolyline($idMap);
				$this->savePolylineLatLng($request,$idPolyline);
				$idPolygon = $this->savePolygon($idMap);
				$this->savePolygonLatLng($request,$idPolygon);
				echo json_encode(array('persistence'=>true));
				
			} catch (Exception $e) {
				echo json_encode(array('persistence'=>false));
			}				
		}

		/**
		*Resgata informaçãos do banco de dados para criar um grid para o usuário
		*TODO addicionar bloco de excessao
		*/
		public function  showAllGridCartaoPrograma()
		{
			$cards = $this->getPersistence()->select(new Card());


			if($cards)
				$this->buildGridCartaoPrograma($cards);
			else
				echo '<div class="alert alert-block">
					   <h4>Ops desculpe!</h4>
	  					Você ainda nao tem nenhum cartão programa cadastrado em nosso sistema. Cadastre agora mesmo <a href="./new" class="btn">Cadastrar</a>
				    	</div>';
		
		}

		/**
		*Resgata informaçãos do banco de dados para criar um grid para o usuário
		*@TODO addicionar bloco de excessao
		*@param $request
		*/
		public function showGridCartaoProgramaByQuerySearch(Request $request)
		{
			if($request->isElement('q'))
			{

				$request->sset('cardDescription',$request->getKey('q'));
				$cards = $this->getPersistence()->selectAllCardWithDescription($request->buildObjectToInstace(new Card()),'cardDescription= LIKE %?%');
				
				if($cards)
				
					$this->buildGridCartaoPrograma($cards);
				else
					echo '<div class="alert alert-block">
						   <h4>Ops desculpe!</h4>
  						 	Sua busca não retornou nenhum valor! Faça uma nova pesquisa para encontrarmos o que deseja
						</div>';
			}
		}


		/**
		*Resgata informaçãos do banco de dados para criar um grid para o usuário
		*@TODO addicionar bloco de excessao
		*@param $request
		*/
		public function showGridCartaoPrograma(Request $request)
		{
			if($request->getKey('q'))
				$this->showGridCartaoProgramaByQuerySearch($request);
			else
				$this->showAllGridCartaoPrograma();
		}


		/**
		*Resgata informaçãos do banco de dados para criar um grid para o usuário
		*@TODO addicionar bloco de excessao
		*@param $card
		*/
		private function buildGridCartaoPrograma($cards)
		{
			echo '<table class="table table-hovered table-bordered"><tr><td>ID</td><td>Descrição do cartão</td><td>Sigilo</td><td>Ação</td></tr>';
			
			foreach ($cards as $card) 
				echo '<tr><td>'.$card->getId().'</td><td>'.$card->getCardDescription().'</td><td>temos que add o sigilo</td><td><a href="./card?idCard='.$card->getId().'">Visualizar</a></td></tr>';
			echo '</table>';
		}


		/**
		*Resgata informaçãos do banco de dados para criar um grid para o usuário
		*@TODO addicionar bloco de excessao
		*@param $request
		*/
		public function showGlobalMarkersCartaoPrograma()
		{
			$global = $this->getPersistence()->selectGlobalMarkers();
			echo json_encode(array('globals'=>$global));
		}	



		/**
		*Resgata informaçãos do banco de dados para criar os marcadores globais
		*@TODO addicionar bloco de excessao
		*@param $request
		*@return json_encode($transac)
		*/
		public function saveGlobalMarker(Request $request)
		{

			$markerGlobal = $request->buildObjectToInstace(new MarkerGlobal());
			$this->getPersistence()->persist($markerGlobal);
			echo json_encode(array('persitence'=> 'true'));
		}

		/** 
		*@TODO avaliar opções para persistência desta transação
		*/
		public function updateMarkerGlobal(Request $request)
		{
			$response = $this->getPersistence()->updateMarkerGlobal(
				$request->buildObjectToInstace(new MarkerGlobal())
				,$request->getKey('originalLat')
				,$request->getKey('originalLng')
			);	

			if($response)
				echo json_encode(array('persistence'=> true));
		}

		/**
		*Remove ponto Global
		*@param $request
		*@return $response)
		*/
		public function removeMarkerGlobal(Request $request)
		{
			$response = $this->getPersistence()->delete($request->buildObjectToInstace(new MarkerGlobal()));
			if($response)
				echo json_encode(array('persistence'=>false));
		}

		/**
		*Resgata informaçãos do banco de dados para enviar dados do cartão programa ao usuário
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return json_encode($transac)
		*/
		public function showExistingCartaoPrograma($idCard)
		{
			echo json_encode($this->buildJsonResponse($idCard));
		}

		/**
		*Agrupa informaçoes do cartão programa para insetir na trasação response
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $merge
		*/
		public function buildJsonResponse($idCard)
		{
			return array_merge(
					$this->buildMarkersJsonResponse($idCard)
					, $this->buildCardJsonResponse($idCard)
					, $this->buildPolygonJsonResponse($idCard)
					, $this->buildPolylineJsonResponse($idCard)
					, $this->buildMapJsonResponse($idCard)
				);	
		}

		/**
		*Realiza a consulta de marcadores do cartão programa
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $maker
		*/
		public function buildMarkersJsonResponse($idCard)
		{
			$markers = $this->getPersistence()->selectMarkers($idCard);
			return array('markers' => $markers);
		}

		/**
		*Realiza a consulta de card do cartão programa
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $card
		*/
		public function buildCardJsonResponse($idCard)
		{
			$card = $this->getPersistence()->selectCard($idCard);
			return $card[0];
		}

		/**
		*Realiza a consulta de polygon do cartão programa
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $polygon
		*/
		public function buildPolygonJsonResponse($idCard)
		{
			$polygon = $this->getPersistence()->selectPolygon($idCard);
			return array('polygon'=> array('path'=>$polygon));
		}

		/**
		*Realiza a consulta de polyline do cartão programa
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $polyline
		*/
		public function buildPolylineJsonResponse($idCard)
		{
			$polyline = $this->getPersistence()->selectPolyline($idCard);
			return array('polyline' => array('path'=>$polyline));
		}

		/**
		*Realiza a consulta de map do cartão programa
		*@TODO addicionar bloco de excessao
		*@param $idCard
		*@return array $map
		*/
		public function buildMapJsonResponse($idCard)
		{
			$map = $this->getPersistence()->selectMap($idCard);
			$center = array('lat'=>$map[0]['lat'],'lng'=>$map[0]['lng']);
			return array('map'=>array('zoom'=>(int)$map[0]['zoom'],'center'=>$center));
		}
	}

?>