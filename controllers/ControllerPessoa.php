<?php

	/**
	*@author #PirateTeam
	*Class responsável por realizaçao a comunicação
	* e transação de informações do usuário
	*/

	
	require dirname(__DIR__).'/vendor/autoload.php';

	class ControllerPessoa
	{

		private $persistenceConnection;

		/**
		*@method getPersistece
		*Tem como propósito retornar a conexão ativa.
		*@return $persistenceConnectio
		*/
		public function getPersistence()
		{	
			if(!$this->persistenceConnection)
				$this->persistenceConnection = new Persistence();
			return $this->persistenceConnection;
		}

		/**
		*@method checkAuthenticate
		*Tem como propósito retornar a conexão ativa.
		*@param $request
		*/
		public function checkAuthenticate(Request $request)
		{
			if($request->isElement('userRequestAccess'))
			{
				/*TODO validate*/
				$user = $request->buildObjectToInstace(new User());
				$user = $this->getPersistence()->selectWithWhere($user
								,'username= ? and password= ?');

				if($user)
				{
					session_start();
						$_SESSION['userId'] = $user->getId();
					session_write_close();
						
					//TODO create redirect to index
						echo '<meta HTTP-EQUIV="Refresh" CONTENT="0;URL=dashboard">';	
					
				}
			}
		}


		/**
		*@method getDataUserAuthenticated
		*Seleciona o usuário no banco de dados
		*@param $request
		*/
		public function getDataUserAuthenticated(Request $request)
		{

			session_start();
			if ($_SESSION) 
				$request->set('id',$_SESSION['userId']);			
			session_write_close();
			$user = $request->buildObjectToInstace(new User());
			$userResponse = $this->getPersistence()->selectWithWhere($user,'id= ?');


			return $userResponse;

		}


		/**
		*@method getDataUserAuthenticated
		*Avalia se a informação do usuário esta presente na sessão
		*/
		public function securityPages()
		{
			session_start();
			if (!$_SESSION['userId']) 
			{
				echo '<script>';
				echo 'alert ("Você precisa estar logado para acessar este recurso!")';
				echo '</script>';
				echo '<meta HTTP-EQUIV="Refresh" CONTENT="0;URL=./">';
				die;
			}
			session_write_close();
		}


	}

 ?>