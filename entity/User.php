<?php
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/

	class User 
	{

		/*@ id*/
		private $id;
		
		private $username;
		private $password;

		public function setId($id)
		{
			$this->id = $id;
		}

		public function setUsername($username)
		{
			$this->username = $username;
		}

		public function setPassword($password)
		{
			$this->password = $password;
		}

		public function getId()
		{
			return $this->id;
		}

		public function getUsername()
		{
			return $this->username;
		}

		public function getPassword()
		{
			return $this->password;
		}
		
	}	

 ?>