<?php
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/
	
	class Polygon
	{
		private $id;
		private $idMap;

		public function setId($id)
		{
			$this->id = $id;
		}

		public function setIdMap($idMap)
		{
			$this->idMap = $idMap;
		}

		public function getId()
		{
			return $this->id;
		}

		public function getIdMap()
		{
			return $this->idMap;
		}
	}
?>