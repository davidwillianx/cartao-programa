<?php
	
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/
	
	class LatLng
	{
		private $id;
		private $lat;
		private $lng;

		public function setId($id)
		{
			$this->id = $id;
		}
		public function setLat($lat)
		{
			$this->lat = $lat;
		}
		public function setLng($lng)
		{
			$this->lng = $lng;
		}

		public function getId()
		{
			return $this->id;
		}
		public function getLat()
		{
			return $this->lat;
		}
		public function getLng()
		{
			return $this->lng;
		}
	}
?>