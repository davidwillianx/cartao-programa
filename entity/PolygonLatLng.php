<?php

	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/
class PolygonLatLng
{

	private $idPolygon;
	private $idLatLng;


	public function setIdPolygon($idPolygon)
	{
		$this->idPolygon = $idPolygon;
	}

	public function setIdLatLng($idLatLng)
	{
		$this->idLatLng = $idLatLng;
	}

	public function getIdPolygon()
	{
		return $this->idPolygon;
	}

	public function getIdLatLng()
	{
		return $this->idLatLng;
	}
}
?>