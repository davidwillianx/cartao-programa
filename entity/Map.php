<?php
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/
	
	class Map {

		private $id;
		private $idLatLng;
		private $zoom;
		
		public function setId($id)
		{
			$this->id = $id;
		}
		public function setIdLatLng($idLatLng)
		{
			$this->idLatLng = $idLatLng;
		}

		public function setZoom($zoom)
		{
			$this->zoom = $zoom;
		}

		public function getId()
		{
			return $this->id;
		}

		public function getIdLatLng()
		{
			return $this->idLatLng;
		}

		public function getZoom()
		{
			return $this->zoom;
		}
	}
 ?>