<?php 
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/

	class Card
	{
		private $id;
		private $areaDescription;
		private $roteDescription;
		private $cardDescription;
		private $created;
		private $modified;
		private $idMap;
		private $idUser;

		public function setId($id)
		{
			$this->id = $id;
		}
		public function setAreaDescription($areaDescription)
		{
			$this->areaDescription = $areaDescription;
		}
		public function setRoteDescription($roteDescription)
		{
			$this->roteDescription = $roteDescription;
		}
		public function setCardDescription($cardDescription)
		{
			$this->cardDescription = $cardDescription;
		}
		public function setCreated($created)
		{
			$this->created = $created;
		}
		public function setModified($modified)
		{
			$this->modified = $modified;
		}
		public function setIdMap($idMap)
		{
			$this->idMap = $idMap;
		}
		public function setIdUser($idUser)
		{
			$this->idUser = $idUser;
		}



		public function getId()
		{
			return $this->id;
		}
		public function getAreaDescription()
		{
			return $this->areaDescription;
		}
		public function getRoteDescription()
		{
			return $this->roteDescription;
		}
		public function getCardDescription()
		{
			return $this->cardDescription;
		}
		public function getCreated()
		{
			return $this->created;
		}
		public function getModified()
		{
			return $this->modified;
		}
		public function getIdMap()
		{
			return $this->idMap;
		}
		public function getIdUser()
		{
			return $this->idUser;
		}
	}

?>