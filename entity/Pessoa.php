<?php
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/

class Pessoa
{
	private $nome;
	private $idade;


	public function setNome($nome)
	{
		$this->nome = $nome;
	}

	public function getNome()
	{
		return $this->nome;
	}

	public function setIdade($idade)
	{
		$this->idade = $idade;
	}

	public function getIdade()
	{
		return $this->idade;
	}
}
?>