<?php
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/

class PolylineLatLng
{

	private $idPolyline;
	private $idLatLng;

	public function setIdPolyline($idPolyline)
	{
		$this->idPolyline = $idPolyline;
	}

	public function setIdLatLng($idLatLng)
	{
		$this->idLatLng = $idLatLng;
	}

	public function getIdPolyline()
	{
		return $this->idPolyline;
	}

	public function getIdLatLng()
	{
		return $this->idLatLng;
	}
}

 ?>