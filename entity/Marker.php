<?php
	
	/** 
	*@author #PirateTeam
	* Class responsável por representar 
	* sua entidade de mesmo nome no banco de dados
	*/

	class Marker
	{
		private $id;
		private $idLatLng;
		private $idMap;
		private $description;
		private $arrival;
		private $leave;
		protected $listGeolocation;
		
		public function setId($id)
		{
			$this->id = $id;
		}

		public function setIdMap($idMap)
		{
			$this->idMap = $idMap;
		}

		public function setDescription($description)
		{
			$this->description = $description;
		}

		public function setArrival($arrival)
		{
			$this->arrival = $arrival;
		}

		public function setLeave($leave)
		{
			$this->leave = $leave;
		}

		public function setIdLatLng($idLatLng)
		{	
			$this->idLatLng = $idLatLng;
		}

		public function setLat($element)
		{
			$this->listGeolocation['lat'] = $element;
		}

		public function setLng($element)
		{
			$this->listGeolocation['lng'] = $element;
		}
		public function getId()
		{
			return $this->id;
		}

		public function getIdMap()
		{
			return $this->idMap;
		}

		public function getDescription()
		{
			return $this->description;
		}
		public function getArrival()
		{
			return $this->arrival;
		}
		public function getLeave()
		{
			return $this->leave;
		}

		public function getIdLatLng()
		{
			return $this->idLatLng;
		}

		public function getLat()
		{
			return $this->listGeolocation['lat'];
		}
		public function getLng()
		{
			return $this->listGeolocation['lng'];
		}
	}

?>