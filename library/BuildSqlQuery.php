<?php
/** 
	*@author #PirateTeam 
	*thanks! AIT-PROEG
	* Class responsável por gerar dinâmicamente algumas transações com o banco de dados.
	*/

class BuildSqlQuery
{
	/**
	*@var representa o objeto que esta sendo referenciado no costrutor para a 
	* realizar a operação de construção do SQL.
	*/
	private $objectReference;

	
	public function BuildSqlQuery($objectInstantiate)
	{
		$this->objectReference = $objectInstantiate;
	}

	/** 
	*@method Gera query de inserção baseada nos valores inseridos no objeto em questão.
	*@return String de inserção.
	*/	
	public function insertQuery()
	{
		$reflectionClass = new reflectionClass($this->objectReference);
		$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);
					
		$stringSave = 'INSERT INTO '.lcfirst($reflectionClass->getName()).'(';


		foreach ($properties as $property) {
			if($property->getName() != 'id')	
				$stringSave .= ' `'.$property->getName().'`,';
		}				
				
		$stringSave = substr($stringSave,0,-1);
		$stringSave .= ' ) VALUES ( ';
				

		foreach ($properties as $property) {
			if($property->getName() != 'id')
				$stringSave .= '? ,';
		}

		$stringSave = substr($stringSave, 0, -1).');';
	
		return $stringSave;
	}

	/** 
	*@method Gera query de seleão baseada no objeto em questão.
	*@return String de seleção.
	*/
	public function selectQuery()
	{
		$reflectionClass = new reflectionClass($this->objectReference);
		$properties = $reflectionClass->getProperties(ReflectionProperty::IS_PRIVATE);

		$stringSave = 'SELECT ';

		foreach ($properties as $property) {
			$stringSave .= ' '.$property->getName().',';
		}		

		$stringSave = substr($stringSave,0,-1);
		$stringSave .= ' FROM '.lcfirst($reflectionClass->getName()).';';

		return $stringSave;
	}

	/** 
	*@method Gera query de inserção baseada objeto em questão filtrada pelo condição como parâmetro.
	*@param Clausula Where.
	*@return String de seleão.
	*/
	public function selectQueryWithWhere($where)
	{
		$stringSave = $this->selectQuery();
		$stringSave = substr($stringSave, 0, -1);
		$stringSave .= ' WHERE '.$where.' ;';

		return $stringSave;
	}

	public function deleteQuery()
	{
		$reflectionClass = new ReflectionClass($this->objectReference);
		$properties = $reflectionClass->getProperties();
		
		$stringDelete = 'DELETE from '.lcfirst($reflectionClass->getName()).' WHERE';

		foreach($properties as $property)
		{	
			$reflectionMethod = new ReflectionMethod(
				$this->objectReference,'get'.ucword($property->getName()));
			
			if($reflectionMethod->invoke())
				$stringDelete .= ' '.$property->getName().' = ? AND';
			
			//Remove And element
			
			
		}
		$stringDelete = substr($stringDelete,0,3);		
		
		return $stringDelete;
	}
}

?>