<?php
	
	/** 
	*@author #PirateTeam David
	*thanks! AIT-PROEG
	* Class respons�vel por gerar din�micamente algumas transa��es com o banco de dados.
	*/


class Request
{
	/** 
	*@var $datarequest: armazena as informa��es transacionais (GET/POST);
	*@see Request:Request
	*/
	private $dataRequest = array();

	/** 
	*@method Request
	*@param none
	*amazena informa��es transacionais (GET/POST)
	*/
	function Request()
	{
		if (!is_null($_GET))
			$this->merge($_GET);

		if(!is_null($_POST))
			$this->merge($_POST);
	}

	/** 
	*@access private
	*@method merge
	*@param $data
	* Reliza��o a opera��o de uni�o entre os objetos transacionais
	*/
	private function merge($data)
	{
		$this->dataRequest = array_merge($this->dataRequest,$data);
	}

	/** 
	*@param $key
	*@access public
	*Para acessar um elemento basta utilizar a chave que o presenta (GET or POST)
	*@return $element or null
	*/
	public function getKey($key)
	{
		if($this->isElement($key))
			return $this->dataRequest[$key];
		return null;
	}
	
	/** 
	*@param $key
	*@param $data
	*@access public
	*Para inserir um valor na requisi��o
	*@return String $message
	*/
	public function set($key,$data)
	{
		if ($this->isElement($key))
			echo '<strong>J� existe um elemento com a chave requisitada<strong>';
		else 
			$this->dataRequest[$key] = $data;
	}
	

	/** 
	*@param $key
	*@param $data
	*@access public
	*Atualiza o valor repsentado por este �ndice
	*/
	public function update($key,$data)
	{
		$this->dataRequest[$key] = $data;
	}
	

	/** 
	*@param $key
	*@param $newKey
	*@access public
	*Atualiza o �ndice para este valor
	*/
	public function releaseKey($key,$newKey)
	{
		$data = $this->getKey($key);
		$this->remove($key);
		$this->set($newKey, $data);
	}


	/** 
	*@param $key
	*@access public
	*Verifica a presen�a do elemento
	*@return true or false
	*/
	public function isElement($key)
	{
		if(isset($this->dataRequest[$key]))
			return true;
		return false;
	}


	/** 
	*@access public
	*Exibe toda informa��o armazenada posteriormente parando a execu��o
	*/
	public function explodeRequest()
	{
		var_dump($this->dataRequest);
		die;
	}
	
	
	/** 
	*@param $key
	*@access public
	*Remove um valor a partir de sua chave
	*@return String $message
	*/
	public function remove($key)
	{
		unset($this->dataRequest[$key]);
	}


	/** 
	*@param $instanceObject
	*@access public
	*Adciona os valores dos �ndices que represent�o atributos no objeto
	*@return $instaceObject  - populado
	*/
	public function  buildObjectToInstace($instanceObject)
	{
		return $this->buildObject($instanceObject,$this->dataRequest);
	}

	/** 
	*@param $instanceObject
	*@param $key
	*@access public
	*Adciona os valores dos �ndices que represent�o atributos no objeto
	*@return $instaceObject  - populado
	*/
	public function buildObjectToKey($instanceObject,$key)
	{
		return $this->buildObject($instanceObject,$this->dataRequest[$key]);
	}

	/** 
	*@param $instanceObject
	*@param $key
	*@access public
	*Adciona os valores dos �ndices que represent�o atributos no objeto
	*@return array $marker
	*/
	public function buildMarkers($instanceObject,$key)
	{
		$dataMarkers = $this->dataRequest[$key];
		$indexList = 0;
		$arrayMarker = array();

		foreach ($dataMarkers as $dataMarker)
		{
			
				$resultMarker =$this->buildObject(new Marker(),$dataMarker);
				$resultMarker->setLat($dataMarker['lat']);
				$resultMarker->setLng($dataMarker['lng']);
				$arrayMarker[] = $resultMarker;		 	
		}
		return $arrayMarker;
	}

	/** 
	*@param $instanceObject
	*@param $arrayData
	*@access private
	*Adciona os valores dos �ndices que represent�o atributos no objeto
	*@return $instaceObject  - populado
	*/
	private function buildObject($isntaceObject,$arrayData)
	{
		$keys =  array_keys($arrayData);

		$ref = new ReflectionClass($isntaceObject);
		$propertiesObject = $ref->getProperties(ReflectionProperty::IS_PRIVATE);
		
		foreach ($propertiesObject as $propertieObject)
		{
			foreach ($keys as $key)
			{
				if($key == $propertieObject->getName())
				{

					$reflectionMethod = new ReflectionMethod($isntaceObject, 'set'.ucwords($key));
					$reflectionMethod->invoke($isntaceObject,$arrayData[$key]);
				}
			}
		}
		return $isntaceObject;
	}




	/** 
	*@param $indexList
	*@access public
	*Cria uma lista de Latitude e Longiture
	*@return array $latLngs
	*/
	public function listLatLng($indexList)
	{
		if($indexList === 'map')
			$listLatLng = $this->dataRequest[$indexList];
		else
			$listLatLng = $this->dataRequest[$indexList]['path'];
		
		$latLngs = array();

		foreach ($listLatLng as $latLngElement) {
			if(is_array($latLngElement))
			{	
				$latLng =  new LatLng();
				$latLng->setLat($latLngElement['lat']);
				$latLng->setLng($latLngElement['lng']);

				$latLngs[] = $latLng; 
			}
		}	
		return $latLngs;
	}

}